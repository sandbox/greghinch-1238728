## Meebo for Drupal - _Updated_

Meebo lets users share and communicate with friends via the Meebo Bar at the bottom of the page of your site. We share revenue with publishers via brand advertising sold by Meebo. Partners running Meebo on their sites see increases in user engagement and audience growth.

### Summary

This module is a continuation of the original Meebo module for Drupal, done by [Ahmad Kharbat](http://drupal.org/user/140400"). The work here is intended to take the module for Drupal 7 out of developer beta and into general release. It has been sponsored by Meebo, who were unable to contact the original developer.

### Installation

1. Download the module and unzip it. 
2. Place that folder inside your Drupal 7 /modules directory. 
3. On the Modules tab of the Drupal Admin, in the section marked Other, enable the Meebo Updated module (if you have the original Meebo module installed, you must disable it)
4. In the Configuration tab of the Drupal Admin, open the Meebo section, and add your Meebo Network Id. To find this Id, navigate to the [Meebo Dashboard](https://dashboard.meebo.com), select or add your site, and copy the portion of the URL that represents your Network Id. Ex: (shown in bold) dashboard.meebo.com/__mysitecom_abc123__/

Alternatively, you can include your Network Id a config file and it will be read in when the module is enabled. Copy the file "config.inc.example" to a file named "config.inc", and change the value of $meebo\_id to be your Network Id.

You should now have a Meebo Bar on your site.

### Configuration

Some configuration can be done from within the Drupal Admin, the rest must be done from the [Meebo Dashboard](https://dashboard.meebo.com). From the Drupal Admin console, you can toggle Sharing functionality and visibility of the Share Button, as well as configure which pages will show the Bar. The Meebo Dashboard will allow you to configure service specific buttons to show on the Bar
